'use strict';
const  utill = require("../../utill.js");
let cartmodel = require('./cartModel.js');


const cartadding = (request, response) => {
    let filledModel = cartmodel.fillData(request.body);
    const newdocument = new cartmodel.model(filledModel);
    newdocument.save((err, cartadd) => {
        if (err) {
            response.json(utill.responseErrorJSON(0, 'error', err));
        }
        response.json(utill.responseSuccessJSON(1, 'success', [cartadd]));
    })
};

module.exports={cartadding}
