const cartController = require('./cartController.js');
const express = require('express');
const router = express.Router();
let accesstoken = require('../../accesstoken.js').verifyAccessToken;
router.post("/cartadd", cartController.cartadding);
// router.get("/getproducts",accesstoken,productController.getallproducts);

module.exports = router;