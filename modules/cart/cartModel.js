'use strict';
let mongoose = require('mongoose');
const Schema = mongoose.Schema;

let cartSchema = new Schema({
    "cartItems": {
        "type": "array",
        "required": true
    },
    "cartTotalAmount": {
        "type": "number",
        "required": true
    },
    "cartQuantity": {
        "type": "number",
        "required": true
    },
    "uid": {
        "type": "string",
        "required": false
    },
    "createdDate": {
        "type": "date",
        "default": Date.now
    },
    "updatedDate": {
        "type": "date",
        "default": Date.now
    }

});


const fillData = function (obj) {
    return Object.assign({}, {
        'uid': obj.uid,
        'cartItems': obj.cartItems,
        'cartTotalAmount': obj.cartTotalAmount,
        'cartQuantity': obj.cartQuantity,
        'updatedDate': obj.updatedDate,
        'createdDate': obj.createdDate
    });
}

module.exports = { 'model': mongoose.model('cart', cartSchema), 'fillData': fillData };