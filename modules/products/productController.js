"use strict";
const  utill = require("../../utill.js");
// let fillData = require('./productModel.js').fillData;
let productModel = require('./productModel.js');
const productadding = (request, response) => {
  let filledModel = productModel.fillData(request.body);
  const newdocument = new productModel.model(filledModel);
  newdocument.save((err, user) => {
        if (err) {
            response.json(utill.responseErrorJSON(401, 'error', err));
        }
        response.json(utill.responseSuccessJSON(200, 'Product added successfully', ));
    })
};
const getallproducts = (request, response) => {
    productModel.find({}, (err, products) => {
      if (err) {
        response.json(utill.responseErrorJSON(0, "error", err));
      }
      response.json(utill.responseSuccessJSON(1, "success", products));
    });
  };
module.exports={productadding,getallproducts}
