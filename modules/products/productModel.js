// 'use strict';
// let mongoose = require('mongoose');
// const Schema = mongoose.Schema;
// let productsSchema = new Schema({

//     productName: {
//         type: String,
//         required: false
//       },
//       price: {
//         type: Number,
//         required: false
//       },
//       colors: {
//         productImages:Array,
//         sizes:Array
//       }
      

// })
// const fillData = function (obj) {
//     return Object.assign({}, {
//       'productName': obj.productName,
//       'price': obj.price,
//       'colors': obj.colors,

//     })
// }

// const products = mongoose.model('products', productsSchema);
// module.exports = { 'model': products, 'fillData': fillData };

// // module.exports = mongoose.model('products', productsSchema);

'use strict';
let mongoose = require('mongoose');
const Schema = mongoose.Schema;

let productsSchema = new Schema({
      "productName": {
        "type": "string",
        "required": "false"
      },
      "price": {
        "type": "number",
        "required": "false"
      },
    "colors": {
      "colorCode":{
        "type": "string",
        "required": "false",
      
      "productImages": {
        "type": "array",
        "required": true
    },
    "sizes": {
      "type": "array",
      "required": true
  },
    },
  }
});

const fillData = function (obj) {
    return Object.assign({}, {
      'productName': obj.productName,
      'price': obj.price,
      'colors': obj.colors,
    })
}

module.exports = { 'model': mongoose.model('products', productsSchema), 'fillData': fillData };