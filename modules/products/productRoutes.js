const productController = require('./productController.js');
const express = require('express');
const router = express.Router();
let accesstoken = require('../../accesstoken.js').verifyAccessToken;
router.post("/productadd", productController.productadding);
router.get("/getproducts",accesstoken,productController.getallproducts);

module.exports = router;