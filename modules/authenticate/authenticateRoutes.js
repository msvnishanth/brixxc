const userController = require('./authenticateController.js');
let express = require('express');
const router = express.Router();
router.post("/signup", userController.signup);
router.post("/signin", userController.Signin);
router.post("/refreshtoken",userController.refreshToken)
module.exports = router;