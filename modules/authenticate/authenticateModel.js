'use strict';
let mongoose = require('mongoose');
const Schema = mongoose.Schema;
let usersSchema = new Schema({
    "email": {
        "type": "string",
        "unique": true,
        "required": true
      },
      "password": {
        "type": "string",
        "required": false
      },
     
      "firstName": {
        "type": "string",
        "required": false
      },
      "lastName": {
        "type": "string",
        "required": false
      },
      "createdDate": {
        "type": "date",
        "default": Date.now
      }

})
const fillData = function (obj) {
    return Object.assign({}, {
      'email': obj.email.toLowerCase(),
      'password': obj.password,
      'firstName': obj.firstName,
      'lastName': obj.firstName,
      'created_date':obj.createdDate

    })
}

const users = mongoose.model('users', usersSchema);
module.exports = { 'model': users, 'fillData': fillData };