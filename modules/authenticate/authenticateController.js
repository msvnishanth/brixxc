"use strict";
const User = require("../authenticate/authenticateModel").model;
const fillData = require("../authenticate/authenticateModel").fillData;
const  utill = require("../../utill.js");
const bcrypt = require('bcrypt');
let generateAccessToken = require('../../accesstoken.js').generateAccessToken;

const signup= (request, response, next) =>{
  let users = {};
  users = request.body;
    User.findOne({ email: users.email.toLowerCase() }, (err, existingemail) => {
        if (err) {
          return next(err);
        }
        if (existingemail) {
          response.json(
            utill.responseErrorJSON(
              0,
              "An account with that email address already exist.",
              "Error"
            )
          );
        } else {
          let filledModel = fillData(users);
          const _model = new User(filledModel);
          if (request.body.hasOwnProperty("password")) {
            let salt = bcrypt.genSaltSync(5);
            let hash = bcrypt.hashSync(users.password, salt);
            _model.password = hash;
          }
          _model.save((err, nuser) => {
            if (err) {
              return next(err);
            }
            response.json(utill.responseSuccessJSON(1, "user registered succefully",));
          });
        }
      });
    };
    function tokens(User){
      const loginuser = {};
      const accessToken = generateAccessToken(loginuser);
      return accessToken;

    }
    const Signin = async(request, response) => {
      console.log(request.body);
      let query = {};

      let pwdError = "Authentication failed. Wrong credentials.";
      let ErrorCotes = "Authentication failed. Wrong credentials.";
      query = { email: request.body.email.toLowerCase() };

      User.findOne(query, (err, user) => {
        if (err) {
          console.log("err", err);
          response.json(utill.responseErrorJSON(0, "error", err));
        }
        if (!user) {
          response.json(utill.responseErrorJSON(0, ErrorCotes, ""));
        } else {
          let isMatch = bcrypt.compareSync(request.body.password, user.password);
          console.log("is match", isMatch);
          if (isMatch) {
            console.log("signin success",);
            const loginuser = {};
            const accessToken = tokens(loginuser)
            loginuser.firstName = user.firstName;
            loginuser.lastName = user.lastName;
            loginuser._id = user._id;
            loginuser.token = accessToken;
            response.json(utill.responseSuccessJSON(1, "success", loginuser));
     
          } else {
            response.json(utill.responseErrorJSON(0, pwdError, ""));
          }
        }
      });
  
    };
  

 
   const  refreshToken = async (req, res) => {
     try{
      const { refreshToken } = req.body;
      const loginuser = {};
      const accessToken =await tokens(loginuser);
      console.log('accessToken',accessToken);
       res.status(1).json({
        accessToken: accessToken,
      });
    return accessToken
    } catch (err) {
      return res.status(0).send({ message: err });
    }
    };
  
   module.exports = {signup,Signin,refreshToken};